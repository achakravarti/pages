(require 'ox-publish)

(setq org-publish-project-alist
      (list
       (list "my-site"
	     :recursive t
	     :base-directory "./src"
	     :publishing-directory "./public"
	     :publishing-function 'org-html-publish-to-html)))

(org-publish-all t)
(message "HTML build complete")
